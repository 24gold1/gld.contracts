const argv = require("yargs/yargs")()
  .env("")
  .options({
    gas: {
      alias: "enableGasReport",
      type: "boolean",
      default: false,
    },
    mode: {
      alias: "compileMode",
      type: "string",
      choices: ["production", "development"],
      default: "development",
    },
    coverage: {
      type: "boolean",
      default: false,
    },
  })
  .argv;

require("@nomiclabs/hardhat-truffle5");
require("@nomiclabs/hardhat-ethers");

if (argv.enableGasReport) {
  require("hardhat-gas-reporter");
}

const withOptimizations = argv.enableGasReport || argv.compileMode === "production";

require("dotenv").config();

const infuraKey = process.env.INFURA_KEY
const coinmarketcap = process.env.COINMARKETCAP;
const mnemonic = process.env.MNEMONIC || "hello hello hello";


/**
 * @type import("hardhat/config").HardhatUserConfig
 */
module.exports = {
  solidity: {
    version: "0.8.0",
    settings: {
      optimizer: {
        enabled: withOptimizations,
        runs: 200,
      },
    },
  },
  networks: {
    ropsten: {
      url: `https://ropsten.infura.io/v3/${infuraKey}`,
      accounts: {
        mnemonic,
      },
    },
    mainnet: {
      url: `https://mainnet.infura.io/v3/${infuraKey}`,
      accounts: {
        mnemonic,
      },
    },
    bsc_testnet: {
      url: "https://data-seed-prebsc-1-s1.binance.org:8545",
      chainId: 97,
      accounts: {
        mnemonic,
      },
    },
    bsc_mainnet: {
      url: "https://bsc-dataseed.binance.org/",
      chainId: 56,
      accounts: {
        mnemonic,
      }
    }
  },
  gasReporter: {
    currency: "USD",
    outputFile: argv.ci ? "gas-report.txt" : undefined,
    coinmarketcap: coinmarketcap
  },
};

if (argv.coverage) {
  require("solidity-coverage");
}


async function getToken(tokenName) {
  const networkName = hre.network.name;
  const tokenConfig = require("./tokenConfig.json")[networkName][tokenName];

  const Token = await ethers.getContractFactory(tokenConfig.contract);
  return await Token.attach(tokenConfig.address);
}

// ERC20 Functions

task("balance", "Gets the balance of an address")
  .addParam("token", "Name of the token")
  .addParam("address", "Address for which to get balance")
  .setAction(async (taskArgs) => {
    const tokenName = taskArgs.token;
    const address = taskArgs.address;

    let balance = 0;
    if (tokenName == "") {
      balance = await ethers.provider
        .getBalance(address)
        .then((balance) => ethers.utils.formatEther(balance));
    } else {
      const token = await getToken(tokenName);
      balance = await token.balanceOf(address);
    }
    console.log(`Balance is: ${balance}`);
});

task("mint", "Mints a token")
  .addParam("token", "Name of the token")
  .addParam("address", "Address to mint to")
  .addParam("amount", "The amount of token")
  .setAction(async (taskArgs) => {
    const token = await getToken(taskArgs.token);
    await token.mint(taskArgs.address, taskArgs.amount);
    console.log(`Minted ${taskArgs.amount} to ${taskArgs.address}`);
});

task("burn", "Burns a token")
  .addParam("token", "Name of the token")
  .addParam("amount", "The amount of token")
  .setAction(async (taskArgs) => {
    const token = await getToken(taskArgs.token);
    await token.burn(taskArgs.amount);
    console.log(`Burned ${taskArgs.amount}`);
});

task("transfer", "Transfer amount from one currency to the other")
  .addParam("token", "Name of the token")
  .addParam("amount", "The amount of token")
  .addParam("address", "Address to mint to")
  .setAction(async (taskArgs) => {
    const token = await getToken(taskArgs.token);
    await token.transfer(taskArgs.address, taskArgs.amount);
    console.log(`Transferred ${taskArgs.amount} to ${taskArgs.address}`);
});

task("approve", "Approves")
  .addParam("token", "Name of the token")
  .addParam("amount", "The amount of token")
  .addParam("address", "Address to mint to")
  .setAction(async (taskArgs) => {
    const token = await getToken(taskArgs.token);
    await token.approve(taskArgs.address, taskArgs.amount);
    console.log(`Approved ${taskArgs.amount} to ${taskArgs.address}`);
});

task("allowance", "Check Allowance")
  .addParam("token", "Name of the token")
  .addParam("owner", "Address of the owner")
  .addParam("spender", "Address of the spender")
  .setAction(async (taskArgs) => {
    const token = await getToken(taskArgs.token);
    const allowance = await token.allowance(taskArgs.owner, taskArgs.spender);
    console.log(`Allowance is: ${allowance}`);
});

task("transferFrom", "Transfer From")
  .addParam("token", "Name of the token")
  .addParam("amount", "The amount of token")
  .addParam("sender", "Address of the sender")
  .addParam("recipient", "Address of the recipient")
  .setAction(async (taskArgs) => {
    const token = await getToken(taskArgs.token);
    await token.transferFrom(taskArgs.sender, taskArgs.recipient, taskArgs.amount);
    console.log(`Transferred ${taskArgs.amount} from ${taskArgs.sender} to ${taskArgs.recipient}`);
});

// FeeCalculator Functions

task("fees", "Set Fees")
  .addParam("percentageRate", "Percentage for fees")
  .addParam("maxFee", "Maximum Fee")
  .setAction(async (taskArgs) => {
    const calculator = await getToken("FeeCalculator");
    await calculator.setParams(taskArgs.percentageRate, taskArgs.maxFee);
    console.log(`Updated FeeCalculated rate to ${taskArgs.percentageRate}% and maxFee to ${taskArgs.maxFee}`)
});

// AddressStatusManager Functions

const ADDRESS_STATUS = {
  0: "Unverified",
  1: "Blacklisted"
};

task("status", "Get the address status manager status")
  .addParam("address", "Address you are trying to check")
  .setAction(async (taskArgs) => {
    const manager = await getToken("AddressStatusManager");
    const status = await manager.statusOf(taskArgs.address);
    console.log(`Status: ${ADDRESS_STATUS[status]}`);
});

task("updateStatus", "Updates the status of an address")
  .addParam("address", "Address you are trying to check")
  .addParam("status", "New Status (int)")
  .setAction(async (taskArgs) => {
    const manager = await getToken("AddressStatusManager");
    const status = await manager.setStatus(taskArgs.address, taskArgs.status);
    console.log(`Updated status for ${taskArgs.address} to ${ADDRESS_STATUS[taskArgs.status]}`);
});

