# 24G - Smart Contracts

Please see below the code for the 24Gold smart contracts.

## Set up

In order to run everything in this folder, you need to have `npm` set up. Next, you can install all dependencies as follows:

```
npm install
```

You can also run the tests using hardhat by running:

```
npm test
```

If you want to generate a gas report, enable the gas reporter:

```
env enableGasReport=true npx hardhat test
```

In a similar way, other parameters from the `hardhat.config.js` file can be changed.

## Contracts

There are a couple main contracts that are important:
1. Token - This is the main token contract. It follows the ERC20 standard
2. FeeCalculator - Responsible for calculating fees
3. AddressStatusManager - Allows us to track KYC status for different addresses
4. Reward - Yield Based token, which can distribute rewards.


## Architecture

Below you can see the architecture of the different smart contracts:

![Architecture Picture](static/architecture_overview.png)

## Interact with Contracts

You can easily interact with different contracts via a set of different commands:

```
npx hardhat balance --token <GLDTF|SLVTF|...> --address 0x...
npx hardhat mint --token  <GLDTF|SLVTF|...> --address 0x... --amount x
npx hardhat burn --token  <GLDTF|SLVTF|...> --amount x
npx hardhat transfer --token  <GLDTF|SLVTF|...> --address 0x... --amount x
npx hardhat approved --token  <GLDTF|SLVTF|...> --address 0x... --amount x
```

