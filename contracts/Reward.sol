// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

import "@openzeppelin/contracts/utils/introspection/IERC165.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

import "./ERC20.sol";
import "./Upgradable.sol";
import "./CustomSafeMath.sol";
import "./AddressStatusManager.sol";

// TODO: Upgradable
// TODO: Check mint & burn dividend functionality

interface IReward {
    function mint(address account, uint256 amount) external;

    function burn(uint256 amount) external;

    function pause() external;

    function unpause() external;

    function setAddressStatusManager(address _addressStatusManager) external;

    function distributeDividend(address token) external;

    function withdrawDividend(address token) external;

    function dividendOf(address token, address owner)
        external
        view
        returns (uint256);

    function withdrawnDividendOf(address token, address owner)
        external
        returns (uint256);

    function accumulativeDividendOf(address token, address owner)
        external
        view
        returns (uint256);
}

contract Reward is IReward, IERC165, ERC20, Pausable {
    using SafeMath for uint256;
    using CustomSafeMath for uint256;
    using CustomSafeMath for int256;

    AddressStatusManager public addressStatusManager;

    uint256 internal constant magnitude = 2**128;
    address[] internal supportedTokens;
    mapping(address => uint256) internal magnifiedDividendPerShare;

    // The way that this logic works is that let's say the balance can never change, then
    // We can calculate the dividend as follows:
    // `dividendOf(user) = dividendPerShare * balanceOf(user)`
    // But the problem arises when people can transfer tokens. If you get a dividend,
    // this belongs to you, even if you transfer the tokens afterwards. To get this behavior
    // we add a correction term.
    // `dividendOf(user) = dividendPerShare * balanceOf(user) + dividendCorrectionOf(user)`.
    // where `dividendCorrectionOf(user)` is updated whenever `balanceOf(user)` is changed.
    // `dividendCorrectionOf(user) = dividendPerShare * (old balanceOf(user)) - (new balanceOf(_user))`.
    // So now `dividendOf(user)` returns the same value before and after `balanceOf(user)` is changed.
    // Now, this yield token is a bit special, because it is generic and can distribute dividends of other tokens.
    // This is why we have a mapping (address => (address => ...)). The first address is the token
    // it will distribute and the second is the address of the user.
    mapping(address => mapping(address => int256))
        internal magnifiedDividendCorrections;
    mapping(address => mapping(address => uint256)) internal withdrawnDividends;

    event DividendsDistributed(address indexed token, uint256 amount);

    event DividendsWithdrawn(
        address indexed token,
        address indexed owner,
        uint256 amount
    );

    /**
     *  Creates a new Twenty Four Smart Reward Token instance
     *
     *  Requirements:
     *   - name should be the name of the token
     *   - symbol is the ticker of the token
     *   - initialSupply is the initial amount of tokens to issue
     */
    constructor(
        string memory name,
        string memory symbol,
        uint256 initialSupply,
        address _addressStatusManager
    ) ERC20(name, symbol) {
        addressStatusManager = AddressStatusManager(_addressStatusManager);

        _mint(msg.sender, initialSupply);
    }

    /**
     * If this contract has become the owner of some of this particular token,
     * The owner can trigger a dividend, meaning that this particular coin will get
     * paid out to the people who hold the yield token.
     * The important thing is that the token has to be conform to the ERC165 & ERC20
     * standard.
     */
    function distributeDividend(address token)
        external
        virtual
        override
        onlyOwner
    {
        require(totalSupply() > 0);

        IERC20 erc20token = IERC20(token);
        uint256 balance = erc20token.balanceOf(address(this));
        if (balance > 0) {
            // We need to maintain a list of tokens that have a dividend to adjust
            // for every transfer
            _addToken(token);
            magnifiedDividendPerShare[token] = magnifiedDividendPerShare[token]
                .add(balance.mul(magnitude) / totalSupply());
            emit DividendsDistributed(token, balance);
        }
    }

    function withdrawnDividendOf(address token, address owner)
        external
        view
        override
        returns (uint256)
    {
        return withdrawnDividends[token][owner];
    }

    function accumulativeDividendOf(address token, address owner)
        external
        view
        override
        returns (uint256)
    {
        return
            (magnifiedDividendPerShare[token]
                .mul(super.balanceOf(owner))
                .toInt256Safe() + magnifiedDividendCorrections[token][owner])
                .toUint256Safe() / magnitude;
    }

    function dividendOf(address token, address owner)
        external
        view
        virtual
        override
        returns (uint256)
    {
        return
            this.accumulativeDividendOf(token, owner).sub(
                withdrawnDividends[token][owner]
            );
    }

    function withdrawDividend(address token) external virtual override {
        uint256 withdrawableDividend = this.dividendOf(token, msg.sender);
        if (withdrawableDividend > 0) {
            withdrawnDividends[token][msg.sender] = withdrawnDividends[token][
                msg.sender
            ].add(withdrawableDividend);
            emit DividendsWithdrawn(token, msg.sender, withdrawableDividend);
            IERC20 erc20token = IERC20(token);
            bool result = erc20token.transfer(msg.sender, withdrawableDividend);
            require(result, "Transfer was not complete");
        }
    }

    function decimals() external view virtual override returns (uint8) {
        return 4;
    }

    function mint(address account, uint256 amount)
        external
        virtual
        override
        onlyOwner
    {
        _mint(account, amount);
    }

    function burn(uint256 amount) external virtual override onlyOwner {
        _burn(contractOwner(), amount);
    }

    function pause() external virtual override onlyOwner {
        _pause();
    }

    function unpause() external virtual override onlyOwner {
        _unpause();
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 /* amount */
    ) internal virtual override {
        require(
            addressStatusManager.statusOf(from) != 1,
            "Transfer: Cannot transfer from blacklisted user"
        );
        require(
            addressStatusManager.statusOf(to) != 1,
            "Transfer: Cannot transfer to blacklisted user"
        );
    }

    function _afterTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual override {
        for (uint256 i = 0; i < supportedTokens.length; i++) {
            address token = supportedTokens[i];
            int256 magCorrection = magnifiedDividendPerShare[token]
                .mul(amount)
                .toInt256Safe();
            magnifiedDividendCorrections[token][from] =
                magnifiedDividendCorrections[token][from] +
                magCorrection;
            magnifiedDividendCorrections[token][to] =
                magnifiedDividendCorrections[token][to] -
                magCorrection;
        }
    }

    function setAddressStatusManager(address _addressStatusManager)
        external
        virtual
        override
        onlyOwner
    {
        addressStatusManager = AddressStatusManager(_addressStatusManager);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        virtual
        override
        returns (bool)
    {
        return
            (interfaceId == type(IERC165).interfaceId) ||
            (interfaceId == type(IERC20).interfaceId);
    }

    function _addToken(address token) internal {
        bool found = false;
        for (uint256 i = 0; i < supportedTokens.length; i++) {
            if (token == supportedTokens[i]) {
                found = true;
                break;
            }
        }
        if (!found) {
            supportedTokens.push(token);
        }
    }
}
