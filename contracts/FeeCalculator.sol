// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "./Ownable.sol";

abstract contract IFeeCalculator is Ownable {
    function calculate(uint256 amount) external view virtual returns (uint256);
}

/**
 * Contract to calculate different fees
 */
contract FeeCalculator is IFeeCalculator {
    using SafeMath for uint256;

    uint256 private _percentageRate = 0;
    uint256 private _maximumFee = 0;

    event ChangeFee(
        address indexed owner,
        uint256 percentageRate,
        uint256 maximumFee
    );

    /**
      Sets the params. The percentage is in bp (so 1 = 0.01%)
     */
    function setParams(uint256 percentageRate, uint256 newMaxFee)
        external
        onlyOwner
    {
        _percentageRate = percentageRate;
        _maximumFee = newMaxFee;

        emit ChangeFee(contractOwner(), _percentageRate, _maximumFee);
    }

    /**
     * Calculates the fee for transfering a specific amount
     */
    function calculate(uint256 amount)
        external
        view
        override
        returns (uint256)
    {
        uint256 fee = amount.mul(_percentageRate).div(10000);
        if (fee > _maximumFee) {
            fee = _maximumFee;
        }
        return fee;
    }
}
