// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

library CustomSafeMath {
    function toUint256Safe(int256 a) internal pure returns (uint256) {
        require(a >= 0, "int should be > 0");
        return uint256(a);
    }

    function toInt256Safe(uint256 a) internal pure returns (int256) {
        int256 b = int256(a);
        require(b >= 0, "int should be > 0");
        return b;
    }
}
