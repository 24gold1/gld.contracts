// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./Ownable.sol";

/**
 * Below we refer to the status, which is just an int, we have several different options such as:
 * 0 - Unverified
 * 1 - Blacklisted
 * ...
 * The reason we just use ints is because an enum is not upgradable
 */

contract AddressStatusManager is Ownable {
    mapping(address => uint8) private _addressStatuses;

    event ChangeAddressStatus(address indexed account, uint8 status);

    function setStatus(address account, uint8 status) external onlyOwner {
        _addressStatuses[account] = status;
        emit ChangeAddressStatus(account, status);
    }

    function statusOf(address account) external view returns (uint8) {
        return _addressStatuses[account];
    }
}
