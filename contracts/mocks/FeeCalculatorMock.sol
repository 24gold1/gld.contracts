// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "../FeeCalculator.sol";

contract FixedFeeCalculator is IFeeCalculator {
    uint256 private _fixedFee = 0;

    constructor(uint256 fee) {
        _fixedFee = fee;
    }

    function calculate(uint256 amount)
        external
        view
        override
        returns (uint256)
    {
        // +- to avoid warning
        return _fixedFee + amount - amount;
    }
}
