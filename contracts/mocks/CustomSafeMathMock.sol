// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "../CustomSafeMath.sol";

contract CustomSafeMathMock {
    function toUint256Safe(int256 a) external pure returns (uint256) {
        return CustomSafeMath.toUint256Safe(a);
    }

    function toInt256Safe(uint256 a) external pure returns (int256) {
        return CustomSafeMath.toInt256Safe(a);
    }
}
