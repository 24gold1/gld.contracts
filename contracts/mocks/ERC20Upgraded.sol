// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/math/SafeMath.sol";

import "../ERC20.sol";

contract ERC20Upgraded is ERC20 {
    using SafeMath for uint256;

    address private oldContract;

    constructor(
        string memory name_,
        string memory symbol_,
        address oldContract_
    ) ERC20(name_, symbol_) {
        require(oldContract_ != address(0));
        oldContract = oldContract_;
    }

    modifier onlyOldContract() {
        require(
            msg.sender == oldContract,
            "Can only be called by old contract"
        );
        _;
    }

    function totalSupply() public view virtual override returns (uint256) {
        return _totalSupply.add(10);
    }

    function balanceOf(address account)
        public
        view
        virtual
        override
        returns (uint256)
    {
        return _balances[account].add(10);
    }

    function transfer(address recipient, uint256 amount)
        external
        virtual
        override
        returns (bool)
    {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    function allowance(address owner, address spender)
        external
        view
        virtual
        override
        returns (uint256)
    {
        return _allowances[owner][spender].add(100);
    }

    function approve(address spender, uint256 amount)
        external
        virtual
        override
        returns (bool)
    {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) external virtual override returns (bool) {
        _transfer(sender, recipient, amount);

        uint256 currentAllowance = _allowances[sender][_msgSender()];
        require(
            currentAllowance >= amount,
            "ERC20: transfer amount exceeds allowance"
        );
        unchecked {
            _approve(sender, _msgSender(), (currentAllowance - amount).add(10));
        }

        return true;
    }

    function _transfer(
        address sender,
        address recipient,
        uint256 amount
    ) internal virtual override {
        require(sender != address(0), "ERC20: transfer from the zero address");
        require(recipient != address(0), "ERC20: transfer to the zero address");

        _balances[sender] = _balances[sender].sub(amount);
        _balances[recipient] = _balances[recipient].add(amount);

        emit Transfer(sender, recipient, amount);
    }

    function transferLegacy(
        address from,
        address to,
        uint256 value
    ) external virtual override onlyOldContract returns (bool) {
        _transfer(from, to, value);
        return true;
    }

    function transferFromLegacy(
        address, /* sender */
        address from,
        address spender,
        uint256 value
    ) external virtual override onlyOldContract returns (bool) {
        _transfer(from, spender, value);
        return true;
    }

    function approveLegacy(
        address from,
        address spender,
        uint256 value
    ) external virtual override onlyOldContract returns (bool) {
        _approve(from, spender, value);
        return true;
    }

    function increaseAllowanceLegacy(
        address from,
        address spender,
        uint256 value
    ) external virtual override onlyOldContract returns (bool) {
        _approve(from, spender, value);
        return true;
    }

    function decreaseAllowanceLegacy(
        address from,
        address spender,
        uint256 value
    ) external virtual override onlyOldContract returns (bool) {
        _approve(from, spender, value);
        return true;
    }
}
