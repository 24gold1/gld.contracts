// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./Ownable.sol";

contract Upgradable is Ownable {
    bool public deprecated = false;
    address public upgradedAddress = address(0);

    event DeprecateContract(address indexed newAddress);

    // deprecate current contract in favour of a new one
    function deprecate(address _upgradedAddress) external onlyOwner {
        require(
            _upgradedAddress != address(0),
            "Upgradable: Cannot transfer to zero address"
        );

        deprecated = true;
        upgradedAddress = _upgradedAddress;
        emit DeprecateContract(_upgradedAddress);
    }
}
