# Git branches Overview and Setup

There are 3 main branches:

* master (Protected and used for tagging purpose)
* stage (Pointed to VM/EC2 for UAT)
* dev (the consolidated development branch for Development merging and testing)

> All the **feature branches** will be fork off from **dev branch**.
> These **feature branches** will be merge into the **dev** and all consolidated code will be tested on **dev branch**. 
> Once **dev branch** is stable, This will be merge into a **stage branch** which will be pointed to the VM/EC2 instance for UAT purpose.
> Before any further merging into **stage branch**, all the code of **stage branch** will be merge to the master and tagged.
> **upgradable-uups branch** having the upgradable contract code. please **Do not** delete this branch. we will keep this for the fututre reference. There is no use as of now.

![Yields Picture](static/git_structure.png)