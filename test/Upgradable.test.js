const { constants, expectEvent, expectRevert } = require("@openzeppelin/test-helpers");
const { ZERO_ADDRESS } = constants;

const { expect } = require("chai");

const Upgradable = artifacts.require("Upgradable");

contract("Upgradable", accounts => {
    beforeEach(async () => {
        this.upgradable = await Upgradable.new({ from: accounts[0] });
    });

    it("Test Check Owner", async () => {
        expect(await this.upgradable.contractOwner()).to.equal(accounts[0]);
    });

    it("Test Initial State", async () => {
        expect(await this.upgradable.deprecated()).to.equal(false);
        expect(await this.upgradable.upgradedAddress()).to.equal(ZERO_ADDRESS);
    });

    it("Deprecate", async () => {
        const deprecate = await this.upgradable.deprecate(accounts[1], { from: accounts[0] });
        expectEvent(deprecate, "DeprecateContract");

        expect(await this.upgradable.deprecated()).to.equal(true);
        expect(await this.upgradable.upgradedAddress()).to.equal(accounts[1]);
    });

    it("Cannot deprecate as non-owner", async () => {
        expectRevert(
            this.upgradable.deprecate(accounts[0], { from: accounts[1] }),
            "Ownable: Only the owner can call this"
        );
    });

    it("Cannot deprecate to zero address", async () => {
        expectRevert(
            this.upgradable.deprecate(ZERO_ADDRESS, { from: accounts[0] }),
            "Upgradable: Cannot transfer to zero address"
        );
    });
});
