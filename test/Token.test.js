const { BN, constants, expectEvent, expectRevert, makeInterfaceId } = require("@openzeppelin/test-helpers");
const { ZERO_ADDRESS } = constants;

const { expect } = require("chai");

const Token = artifacts.require("Token");
const FeeCalculator = artifacts.require("FeeCalculator");
const AddressStatusManager = artifacts.require("AddressStatusManager");
const TokenV2 = artifacts.require("TokenUpgradeMock");

const name = "Gold Token";
const symbol = "GLDT";
const initialSupply = new BN(10000);
const updatedSupply = new BN(1000);

const {
  shouldBehaveLikeToken,
  shouldBehaveLikeTokenUpgrade
} = require("./Token.behavior");


const {
  shouldBehaveLikeERC20,
  upgradableTests,
  negativeTests,
} = require("./ERC20.behavior");

contract("Token", function(accounts) {
    let testType = "onlyOld";
    beforeEach(async function() {
        this.calculator = await FeeCalculator.new({ from: accounts[1] });
        this.manager = await AddressStatusManager.new({ from: accounts[2] });
        if( testType == "onlyOld" || testType == "upgraded"){
            this.token = await Token.new(
                name,
                symbol,
                initialSupply,
                this.manager.address,
                this.calculator.address,
                { from: accounts[0] }
            );
        }

        if( testType == "onlyNew" || testType == "upgraded"){
            this[testType == "onlyNew" ? "token" : "upgraded"] = await TokenV2.new(
                name,
                symbol,
                updatedSupply,
                this.manager.address,
                this.calculator.address,
                this.token.address,
                { from: accounts[0] }
            );
        }

        if(testType == "upgraded") {
            const deprecate = await this.token.deprecate(this.upgraded.address);
            expectEvent(deprecate, "DeprecateContract");
        }

    });

    it('Should succeed in creating over 2^256 - 1 (max) tokens', async function() {
        // 2^256 - 1
        const initialSupplyAmount = '115792089237316195423570985008687907853269984665640564039457584007913129639935';
        const token = await Token.new(
            name,
            symbol,
            initialSupplyAmount,
            this.manager.address,
            this.calculator.address,
             { from: accounts[0] });
        const totalSupply = await token.totalSupply();
        assert.strictEqual(totalSupply.toString(), '115792089237316195423570985008687907853269984665640564039457584007913129639935');
    });

    describe('Test Old Token', function() {
        shouldBehaveLikeToken(accounts, initialSupply, 0, 0);
        negativeTests("Token", accounts);
        // last 2 params are adjustment and upgradedallowance respectively. 
        // These are parametarized to match the hardcoded value in Mock contracts.
        shouldBehaveLikeERC20("ERC20", initialSupply, accounts[0], accounts[1], accounts[2], 0, 0);
    });  

    describe('Test New Token', function() {
        before(function(){
            testType = "onlyNew";
        });

        shouldBehaveLikeToken(accounts, updatedSupply, 10, 100);
        // last 2 params are adjustment and upgradedallowance respectively. 
        // These are parametarized to match the hardcoded value in Mock contracts.
        shouldBehaveLikeERC20("ERC20", updatedSupply, accounts[0], accounts[1], accounts[2], 10, 100);
        
    });


    describe('Test Upgrade after deployment of New Token', function() {
        before(function(){
            testType = "upgraded";
        });
        // last 3 params are adjustment, upgradedallowance and additionMint respectively. 
        // These are parametarized to match the hardcoded value in Mock contracts.        
        upgradableTests(updatedSupply, accounts[0], accounts[1], 10, 100, 10);
        shouldBehaveLikeTokenUpgrade(updatedSupply, accounts[0], accounts[1], 10, 100, 10);
        negativeTests("Token", accounts);
    });
});

// TODO: Test short attack