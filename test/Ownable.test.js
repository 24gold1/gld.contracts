const { constants, expectEvent, expectRevert } = require("@openzeppelin/test-helpers");
const { ZERO_ADDRESS } = constants;

const { expect } = require("chai");

const Ownable = artifacts.require("Ownable");

contract("Ownable", accounts => {
    beforeEach(async () => {
        this.ownable = await Ownable.new({ from: accounts[0] });
    });

    it("Test Check Owner", async () => {
        expect(await this.ownable.contractOwner()).to.equal(accounts[0]);
    });
    describe("Transfer Ownership", () => {
        it("Changes owner after transfer", async () => {
            const receipt = await this.ownable.transferOwnership(accounts[1], { from: accounts[0] });
            expectEvent(receipt, "OwnershipTransferred");

            expect(await this.ownable.contractOwner()).to.equal(accounts[1]);
        });

        it("Prevents non-owners from transferring", async () => {
            await expectRevert(
                this.ownable.transferOwnership(accounts[1], { from: accounts[1] }),
                "Ownable: Only the owner can call this",
            );
        });

        it("Guards ownership against stuck state", async () => {
            await expectRevert(
                this.ownable.transferOwnership(ZERO_ADDRESS, { from: accounts[0] }),
                "Ownable: new owner is the zero address",
            );
        });
    });
});