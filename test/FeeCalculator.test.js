const { BN, expectEvent, expectRevert } = require("@openzeppelin/test-helpers");

const { expect } = require("chai");

const FeeCalculator = artifacts.require("FeeCalculator");

contract("FeeCalculator", accounts => {
    beforeEach(async () => {
        this.calculator = await FeeCalculator.new({ from: accounts[0] });
    });

    it("Test Check Owner", async () => {
        expect(await this.calculator.contractOwner()).to.equal(accounts[0]);
    });
    it("Test Initial 0 fees", async () => {
        expect(await this.calculator.calculate(100)).to.be.bignumber.equal(new BN("0"))
    });
    describe("Changing initial fees", () => {
        it("Change fees", async () => {
            const receipt = await this.calculator.setParams(100, 20, { from: accounts[0] });
            expectEvent(receipt, "ChangeFee");
            
            // 1% of 1000 is 10
            expect(await this.calculator.calculate(1000)).to.be.bignumber.equal(new BN("10"))
            // Capped at 20
            expect(await this.calculator.calculate(10000)).to.be.bignumber.equal(new BN("20"))
        });
        it("Prevents non-owners from transferring", async () => {
            await expectRevert(
                this.calculator.setParams(1000, 1000,  { from: accounts[1] }),
                "Ownable: Only the owner can call this",
            );
        });
    });

    describe("Negative test cases", async () => {
        const maxFee = 10e15;
        const floatNumber = 1.2;
        it("When percentage fee is in decimal", async () => {
          await expectRevert(
            this.calculator.setParams(floatNumber, 20, { from: accounts[1] }),
            `underflow (fault="underflow", operation="BigNumber.from", value=${floatNumber}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
          );
        });
  
        it("When percentage fee is in negative", async () => {
          const negPercentage = -1.2;
          await expectRevert(
            this.calculator.setParams(negPercentage, 20, { from: accounts[1] }),
            `underflow (fault="underflow", operation="BigNumber.from", value=${negPercentage}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
          );
        });
  
        it("When maximum fee is in decimal", async () => {
          await expectRevert(
            this.calculator.setParams(1, floatNumber, { from: accounts[1] }),
            `underflow (fault="underflow", operation="BigNumber.from", value=${floatNumber}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
          );
        });
  
        it("When maximum fee is a big Number", async () => {
          await expectRevert(
            this.calculator.setParams(1, maxFee, { from: accounts[1] }),
            `overflow (fault="overflow", operation="BigNumber.from", value=${maxFee}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
          );
        });
  
        it("When percentage fee is a big Number", async () => {
          await expectRevert(
            this.calculator.setParams(maxFee, 20, { from: accounts[1] }),
            `overflow (fault="overflow", operation="BigNumber.from", value=${maxFee}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
          );
        });
  
        it("When maximum fee is in negative", async () => {
          const negMaxFee = -2;
          await expectRevert(
            this.calculator.setParams(1, negMaxFee, { from: accounts[1] }),
            `value out-of-bounds (argument="newMaxFee", value=${negMaxFee}, code=INVALID_ARGUMENT, version=abi/5.0.7)`,
          );
        });
      });
});
