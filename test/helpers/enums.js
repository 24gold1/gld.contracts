const { BN } = require('@openzeppelin/test-helpers');

function Enum (...options) {
  return Object.fromEntries(options.map((key, i) => [ key, new BN(i) ]));
}

module.exports = {
  Enum,
  // Below is how you add an enum
  // AddressStatus: Enum(
  //     "UNVERIFIED",
  //     "VERIFIED",
  //     "BLACKLISTED",
  // )
};