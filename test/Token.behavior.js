const { BN, constants, expectEvent, expectRevert, makeInterfaceId } = require('@openzeppelin/test-helpers');
const { expect } = require('chai');
const { ZERO_ADDRESS } = constants;

const FeeCalculator = artifacts.require("FeeCalculator");
const AddressStatusManager = artifacts.require("AddressStatusManager");

function shouldBehaveLikeToken (accounts, initialSupply, adjustment, upgradedallowance) {
    it("Test Check Owner", async function() {
        expect(await this.token.contractOwner()).to.equal(accounts[0]);
    });

    it("Test Supply", async function() {
        expect(await this.token.totalSupply()).to.be.bignumber.equal(new BN(initialSupply).addn(adjustment));
        expect(await this.token.balanceOf(accounts[0])).to.be.bignumber.equal(new BN(initialSupply).addn(adjustment));
    });

    it("Test Name(s)", async function() {
        expect(await this.token.name()).to.equal("Gold Token");
        expect(await this.token.symbol()).to.equal("GLDT");
    });

    it("Test Contract Addresses", async function() {
        let calculator = await this.token.feeCalculator();
        let manager = await this.token.addressStatusManager();

        expect(calculator).to.equal(this.calculator.address);
        expect(manager).to.equal(this.manager.address);
    });

    it("Transfer of Token should not be processed when the public address of recipient is zero", async function() {
        await expectRevert(
            this.token.transfer(ZERO_ADDRESS, 100),
            "ERC20: transfer to the zero address"
        );
    });

    it("Test Basic Transfer (No Fees)", async function() {
        const receipt = await this.token.transfer(accounts[3], 20, { from: accounts[0] });
        expectEvent(receipt, "Transfer");

        expect(await this.token.balanceOf(accounts[0])).to.be.bignumber.equal(new BN(initialSupply).subn(20).addn(adjustment).addn(adjustment));
        expect(await this.token.balanceOf(accounts[3])).to.be.bignumber.equal(new BN("20").addn(adjustment));
    });

    it("Test Basic Transfer with Fee", async function() {
        const receipt = await this.calculator.setParams(1000, 20, { from: accounts[1] });
        expectEvent(receipt, "ChangeFee");

        const transferReceipt = await this.token.transfer(accounts[3], 20, { from: accounts[0] });
        expectEvent(transferReceipt, "Transfer");

        const feeCalculation = parseInt(new BN(20).muln(10).divn(100));
        const feeAmount = feeCalculation;

        if (feeAmount > 20) {
            feeAmount = 20;
        }

        expect(await this.token.balanceOf(accounts[0])).to.be.bignumber.equal(new BN(initialSupply).addn(adjustment).addn(adjustment).subn(20));
        expect(await this.token.balanceOf(accounts[3])).to.be.bignumber.equal(new BN("20").addn(adjustment).subn(feeAmount));
        expect(await this.token.balanceOf(accounts[1])).to.be.bignumber.equal(new BN(feeAmount).addn(adjustment));
    });

    it("Test Basic Transfer Blacklist", async function() {
        const receipt = await this.manager.setStatus(
            accounts[3],
            new BN("1"),
            { from: accounts[2] }
        );
        expectEvent(receipt, "ChangeAddressStatus");

        await expectRevert(
            this.token.transfer(accounts[3], 20, { from: accounts[0] }),
            "Transfer: Cannot transfer to blacklisted user",
        );

        const receipt2 = await this.manager.setStatus(
            accounts[0],
            new BN("1"),
            { from: accounts[2] }
        );
        expectEvent(receipt2, "ChangeAddressStatus");

        await expectRevert(
            this.token.transfer(accounts[4], 20, { from: accounts[0] }),
            "Transfer: Cannot transfer from blacklisted user",
        );
    });

    it("Test Basic TransferFrom (No Fees)", async function() {
        const approval = await this.token.approve(accounts[3], 100, { from: accounts[0] });
        expectEvent(approval, "Approval");

        expect(await this.token.allowance(accounts[0], accounts[3])).to.be.bignumber.equal(new BN("100").addn(upgradedallowance));

        const receipt = await this.token.transferFrom(accounts[0], accounts[4], 20, { from: accounts[3] });
        expectEvent(receipt, "Transfer");

        amount = new BN("100").addn(upgradedallowance).subn(20).addn(adjustment);
        expect(await this.token.allowance(accounts[0], accounts[3])).to.be.bignumber.equal(amount);

        expect(await this.token.balanceOf(accounts[0])).to.be.bignumber.equal(new BN(initialSupply).addn(adjustment).addn(adjustment).subn(20));
        expect(await this.token.balanceOf(accounts[4])).to.be.bignumber.equal(new BN("20").addn(adjustment));
    });

    it("Test Basic TransferFrom with Fee", async function() {
        const approval = await this.token.approve(accounts[3], 20, { from: accounts[0] });
        expectEvent(approval, "Approval");

        const receipt = await this.calculator.setParams(1000, 20, { from: accounts[1] });
        expectEvent(receipt, "ChangeFee");

        const transferReceipt = await this.token.transferFrom(accounts[0], accounts[3], 20, { from: accounts[3] });
        expectEvent(transferReceipt, "Transfer");

        const feeCalculation = parseInt(new BN(20).muln(10).divn(100));
        const feeAmount = feeCalculation;

        if (feeAmount > 20) {
            feeAmount = 20;
        }

        expect(await this.token.balanceOf(accounts[0])).to.be.bignumber.equal(new BN(initialSupply).subn(20).addn(adjustment).addn(adjustment));
        expect(await this.token.balanceOf(accounts[3])).to.be.bignumber.equal(new BN("20").subn(feeAmount).addn(adjustment));
        expect(await this.token.balanceOf(accounts[1])).to.be.bignumber.equal(new BN(feeAmount).addn(adjustment));
    });

    it("Test Basic TransferFrom Blacklist", async function() {
        const approval = await this.token.approve(accounts[3], 20, { from: accounts[0] });
        expectEvent(approval, "Approval");

        const receipt = await this.manager.setStatus(
            accounts[3],
            new BN("1"),
            { from: accounts[2] }
        );
        expectEvent(receipt, "ChangeAddressStatus");

        await expectRevert(
            this.token.transferFrom(accounts[0], accounts[3], 20, { from: accounts[3] }),
            "Transfer: Cannot transfer to blacklisted user",
        );

        const receipt2 = await this.manager.setStatus(
            accounts[0],
            new BN("1"),
            { from: accounts[2] }
        );
        expectEvent(receipt2, "ChangeAddressStatus");

        await expectRevert(
            this.token.transfer(accounts[4], 20, { from: accounts[0] }),
            "Transfer: Cannot transfer from blacklisted user",
        );
    });
    describe("Pausing Logic", () => {
        it("Only Owner Can Pause & Unpause", async function() {
            const paused = await this.token.pause({ from: accounts[0] });
            expectEvent(paused, "Paused");

            const unpaused = await this.token.unpause({ from: accounts[0] });
            expectEvent(unpaused, "Unpaused");

            await expectRevert(
                this.token.pause({ from: accounts[1] }),
                "Ownable: Only the owner can call this",
            );

            await expectRevert(
                this.token.unpause({ from: accounts[1] }),
                "Ownable: Only the owner can call this",
            );
        });

        it("Transfer", async function() {
            const paused = await this.token.pause({ from: accounts[0] });
            expectEvent(paused, "Paused");

            await expectRevert(
                this.token.transfer(accounts[4], 20, { from: accounts[0] }),
                "Pausable: paused",
            );
        });

        it("TransferFrom", async function() {
            const paused = await this.token.pause({ from: accounts[0] });
            expectEvent(paused, "Paused");

            const approval = await this.token.approve(accounts[3], 100, { from: accounts[0] });
            expectEvent(approval, "Approval");

            expect(await this.token.allowance(accounts[0], accounts[3])).to.be.bignumber.equal(new BN("100").addn(upgradedallowance));
            await expectRevert(
                this.token.transferFrom(accounts[0], accounts[4], 20, { from: accounts[3] }),
                "Pausable: paused",
            );
        });
    });

    describe("Minting & Burning Logic", () => {
        it("Minting", async function() {
            const mint = await this.token.mint(accounts[0], 100, { from: accounts[0] });
            expectEvent(mint, "Transfer");

            expect(await this.token.totalSupply()).to.be.bignumber.equal(new BN(initialSupply).addn(100).addn(adjustment));
            expect(await this.token.balanceOf(accounts[0])).to.be.bignumber.equal(new BN(initialSupply).addn(100).addn(adjustment));

            const mint2 = await this.token.mint(accounts[1], 200, { from: accounts[0] });
            expectEvent(mint2, "Transfer");

            expect(await this.token.totalSupply()).to.be.bignumber.equal(new BN(initialSupply).addn(300).addn(adjustment));
            expect(await this.token.balanceOf(accounts[0])).to.be.bignumber.equal(new BN(initialSupply).addn(100).addn(adjustment));
            expect(await this.token.balanceOf(accounts[1])).to.be.bignumber.equal(new BN("200").addn(adjustment));
        });

        it("Burn", async function() {
            const burn = await this.token.burn(1000, { from: accounts[0] });
            expectEvent(burn, "Transfer");

            expect(await this.token.totalSupply()).to.be.bignumber.equal(new BN(initialSupply).subn(1000).addn(adjustment));
            expect(await this.token.balanceOf(accounts[0])).to.be.bignumber.equal(new BN(initialSupply).subn(1000).addn(adjustment));

            await expectRevert(this.token.burn(initialSupply, { from: accounts[0] }),
                "ERC20: burn amount exceeds balance",
            );
        });

        it("Only Owner Can Mint & Burn", async function() {
            const mint = await this.token.mint(accounts[0], 100, { from: accounts[0] });
            expectEvent(mint, "Transfer");

            const burn = await this.token.burn(100, { from: accounts[0] });
            expectEvent(burn, "Transfer");

            await expectRevert(
                this.token.mint(accounts[1], 100, { from: accounts[1] }),
                "Ownable: Only the owner can call this",
            );

            await expectRevert(
                this.token.burn(100, { from: accounts[1] }),
                "Ownable: Only the owner can call this",
            );
        });
        it("Minting of Token should not be processed when the public address is zero ", async function() {
            await expectRevert(
                this.token.mint(ZERO_ADDRESS, 100),
                "ERC20: mint to the zero address"
            );
        });
        it("Rejects burning more than balance", async function() {
            const updatedSupply = new BN(initialSupply).addn(1);
            await expectRevert(
                this.token.burn(updatedSupply),
                "ERC20: burn amount exceeds balance",
            );
        });
        it("Emit Transfer event while minting the token", async function() {
            const receipt = await this.token.mint(accounts[1], new BN(10));
            expectEvent(receipt, "Transfer", {
                from: ZERO_ADDRESS,
                to: accounts[1],
            });
        });
        it("Minting should not be done if end user account is blacklisted", async function() {
            const receipt = await this.manager.setStatus(
                accounts[3],
                new BN("1"),
                { from: accounts[2] }
            );
            expectEvent(receipt, "ChangeAddressStatus");
            await expectRevert(
                this.token.mint(accounts[3], 20, { from: accounts[0] }),
                "Transfer: Cannot transfer to blacklisted user"
            );
        });

    });

    describe("AddressStatusManager Management", () => {
        it("Only Owner Can Update", async function() {
            expect(await this.token.addressStatusManager()).to.equal(this.manager.address);

            let newManager = await AddressStatusManager.new({ from: accounts[5] });
            await this.token.setAddressStatusManager(newManager.address, { from: accounts[0] });
            expect(await this.token.addressStatusManager()).to.equal(newManager.address);

            await expectRevert(
                this.token.setAddressStatusManager(this.manager.address, { from: accounts[5] }),
                "Ownable: Only the owner can call this",
            );
        });

        it("Update StatusManager", async function() {
            const receipt = await this.manager.setStatus(
                accounts[3],
                new BN("1"),
                { from: accounts[2] }
            );
            expectEvent(receipt, "ChangeAddressStatus");

            await expectRevert(
                this.token.transfer(accounts[3], 20, { from: accounts[0] }),
                "Transfer: Cannot transfer to blacklisted user",
            );

            let newManager = await AddressStatusManager.new({ from: accounts[5] });
            await this.token.setAddressStatusManager(newManager.address, { from: accounts[0] });

            // We should be able to transfer since we have a new status manager
            const transfer = await this.token.transfer(accounts[3], 20, { from: accounts[0] });
            expectEvent(transfer, "Transfer");
            expect(await this.token.balanceOf(accounts[0])).to.be.bignumber.equal(new BN(initialSupply).subn(20).addn(adjustment).addn(adjustment));
            expect(await this.token.balanceOf(accounts[3])).to.be.bignumber.equal(new BN("20").addn(adjustment));
        });
    });

    describe("FeeCalculator Management", () => {
        it("Only Owner Can Update", async function() {
            expect(await this.token.feeCalculator()).to.equal(this.calculator.address);

            let newCalculator = await FeeCalculator.new({ from: accounts[5] });
            await this.token.setFeeCalculator(newCalculator.address, { from: accounts[0] });
            expect(await this.token.feeCalculator()).to.equal(newCalculator.address);

            await expectRevert(
                this.token.setFeeCalculator(this.calculator.address, { from: accounts[5] }),
                "Ownable: Only the owner can call this",
            );
        });

        it("Update FeeCalculator", async function() {
            const receipt = await this.calculator.setParams(100, 10, { from: accounts[1] });
            expectEvent(receipt, "ChangeFee");

            const transfer = await this.token.transfer(accounts[3], 100, { from: accounts[0] });
            expectEvent(transfer, "Transfer");
            
            const feeCalculation = parseInt(new BN(100).muln(1).divn(100));
            const feeAmount = feeCalculation

            if (feeAmount > 10) {
                feeAmount = 10;
            }

            expect(await this.token.balanceOf(accounts[0])).to.be.bignumber.equal(new BN(initialSupply).subn(100).addn(adjustment).addn(adjustment));
            expect(await this.token.balanceOf(accounts[3])).to.be.bignumber.equal(new BN("100").subn(feeAmount).addn(adjustment));
            expect(await this.token.balanceOf(accounts[1])).to.be.bignumber.equal(new BN(feeAmount).addn(adjustment));

            let newCalculator = await FeeCalculator.new({ from: accounts[5] });
            await this.token.setFeeCalculator(newCalculator.address, { from: accounts[0] });

            // No more fees now
            const transfer2 = await this.token.transfer(accounts[3], 100, { from: accounts[0] });
            expectEvent(transfer2, "Transfer");

            const anotherFeeCalculation = parseInt(new BN(100).muln(1).divn(100));
            const anotherFeeAmount = anotherFeeCalculation

            if (anotherFeeAmount > 10) {
                anotherFeeAmount = 10;
            }
            expect(await this.token.balanceOf(accounts[0])).to.be.bignumber.equal(new BN(initialSupply).addn(adjustment).subn(200).addn(adjustment).addn(adjustment));
            expect(await this.token.balanceOf(accounts[3])).to.be.bignumber.equal(new BN(200).addn(adjustment).subn(anotherFeeAmount));
            expect(await this.token.balanceOf(accounts[1])).to.be.bignumber.equal(new BN(anotherFeeAmount).addn(adjustment));
        });


    });
    describe("Transfer Fee Test Cases", () => {
        it("When Percentage fee more than maximum Fee", async function() {
            const receipt = await this.calculator.setParams(100, 5, { from: accounts[1] });
            expectEvent(receipt, "ChangeFee");
            const transferReceipt = await this.token.transfer(accounts[3], 1000, { from: accounts[0] });
            expectEvent(transferReceipt, "Transfer");
            expect(await this.token.balanceOf(accounts[1])).to.be.bignumber.equal(new BN("5").addn(adjustment));
        });
        it("When Percentage fee less than maximum Fee", async function() {
            const receipt = await this.calculator.setParams(100, 20, { from: accounts[1] });
            expectEvent(receipt, "ChangeFee");
            const transferReceipt = await this.token.transfer(accounts[3], 1000, { from: accounts[0] });
            expectEvent(transferReceipt, "Transfer");
            expect(await this.token.balanceOf(accounts[1])).to.be.bignumber.equal(new BN("10").addn(adjustment));
        });

        it("When Percentage fee is zero", async function() {
            const receipt = await this.calculator.setParams(0, 20, { from: accounts[1] });
            expectEvent(receipt, "ChangeFee");
            const transferReceipt = await this.token.transfer(accounts[3], 1000, { from: accounts[0] });
            expectEvent(transferReceipt, "Transfer");
            expect(await this.token.balanceOf(accounts[3])).to.be.bignumber.equal(new BN("1000").addn(adjustment));
            expect(await this.token.balanceOf(accounts[0])).to.be.bignumber.equal(new BN(initialSupply).addn(adjustment).addn(adjustment).subn(1000));
            //no fees
            expect(await this.token.balanceOf(accounts[1])).to.be.bignumber.equal(new BN("0").addn(adjustment));
        });

        it("When maximum fee is zero", async function() {
            const receipt = await this.calculator.setParams(100, 0, { from: accounts[1] });
            expectEvent(receipt, "ChangeFee");
            const transferReceipt = await this.token.transfer(accounts[3], 1000, { from: accounts[0] });
            expectEvent(transferReceipt, "Transfer");
            expect(await this.token.balanceOf(accounts[3])).to.be.bignumber.equal(new BN("1000").addn(adjustment));
            expect(await this.token.balanceOf(accounts[0])).to.be.bignumber.equal(new BN(initialSupply).subn(1000).addn(adjustment).addn(adjustment));
            //no fees
            expect(await this.token.balanceOf(accounts[1])).to.be.bignumber.equal(new BN("0").addn(adjustment));
        });
    });

    it("Supports ERC20 interface", async function() {
        const interfaces = {
            ERC165: [
                "supportsInterface(bytes4)",
            ],
            ERC20: [
                "totalSupply()",
                "balanceOf(address)",
                "transfer(address,uint256)",
                "allowance(address,address)",
                "approve(address,uint256)",
                "transferFrom(address,address,uint256)",
            ],
            OTHER: [
                "nonExistingTest()",
            ]
        };

        let interfaceIds = {};
        let fnSignatures = {};
        for (const k of Object.getOwnPropertyNames(interfaces)) {
            interfaceIds[k] = makeInterfaceId.ERC165(interfaces[k]);
            for (const fnName of interfaces[k]) {
                // the interface id of a single function is equivalent to its function signature
                fnSignatures[fnName] = makeInterfaceId.ERC165([fnName]);
            }
        }
        expect(await this.token.supportsInterface(interfaceIds.ERC165)).to.equal(true);
        expect(await this.token.supportsInterface(interfaceIds.ERC20)).to.equal(true);
        expect(await this.token.supportsInterface(interfaceIds.OTHER)).to.equal(false);
    });
}

function shouldBehaveLikeTokenUpgrade(updatedSupply, initialHolder, recipient, adjustment, upgradedallowance, additionMint){
    it("Test balanceOf", async function () {
        expect(await this.token.balanceOf(initialHolder)).to.be.bignumber.equal(new BN(updatedSupply).addn(adjustment));
        expect(await this.token.balanceOf(recipient)).to.be.bignumber.equal(new BN(adjustment));
        
        //this.token is depricated that why we are calling this.upgraded for minting as it"s adminonly function.
        const mintEvent = await this.upgraded.mint(initialHolder, 1000, { from: initialHolder });
        expectEvent(mintEvent, "Transfer");
    
        totalBalance = new BN(updatedSupply).addn(1000).addn(adjustment);
    
        expect(await this.token.balanceOf(initialHolder)).to.be.bignumber.equal(totalBalance);
        expect(await this.token.balanceOf(recipient)).to.be.bignumber.equal(new BN(adjustment));
    });

    it("Transfer", async function () {
        const mintEvent = await this.upgraded.mint(initialHolder, 1000, { from: initialHolder });
        expectEvent(mintEvent, "Transfer");
    
        totalBalance = new BN(updatedSupply).addn(1000).addn(adjustment);
    
        expect(await this.token.balanceOf(initialHolder)).to.be.bignumber.equal(new BN(totalBalance));
    
        const transferEvent = await this.token.transfer(recipient, 500, { from: initialHolder });
        expectEvent(transferEvent, "Transfer");
        expect(await this.token.balanceOf(initialHolder)).to.be.bignumber.equal(new BN(totalBalance).subn(500).addn(additionMint));
        expect(await this.token.balanceOf(recipient)).to.be.bignumber.equal(new BN(500).addn(adjustment));
    });
    
    it("TransferFrom", async function () {
        const mintEvent = await this.upgraded.mint(initialHolder, 1000, { from: initialHolder });
        expectEvent(mintEvent, "Transfer");

        totalBalance = new BN(updatedSupply).addn(1000).addn(adjustment);

        expect(await this.token.balanceOf(initialHolder)).to.be.bignumber.equal(new BN(totalBalance));

        const transferEvent = await this.token.transferFrom(initialHolder, recipient, 500, { from: initialHolder });
        expectEvent(transferEvent, "Transfer");
        expect(await this.token.balanceOf(initialHolder)).to.be.bignumber.equal(new BN(totalBalance).subn(500).addn(additionMint));
        expect(await this.token.balanceOf(recipient)).to.be.bignumber.equal(new BN(500).addn(adjustment));
    });
}

module.exports = {
  shouldBehaveLikeToken,
  shouldBehaveLikeTokenUpgrade
};