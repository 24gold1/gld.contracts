const { BN, constants, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');
const { expect } = require('chai');
const { ZERO_ADDRESS } = constants;

function shouldBehaveLikeERC20(errorPrefix, initialSupply, initialHolder, recipient, anotherAccount, adjustment, upgradedallowance) {
  describe("total supply", function () {
    it("returns the total amount of tokens", async function () {
      expect(await this.token.totalSupply()).to.be.bignumber.equal(new BN(initialSupply).addn(adjustment));
    });
  });

  describe("balanceOf", function () {
    describe("when the requested account has no tokens", function () {
      it("returns zero", async function () {
        expect(await this.token.balanceOf(anotherAccount)).to.be.bignumber.equal(new BN("0").addn(adjustment));
      });
    });

    describe("when the requested account has some tokens", function () {
      it("returns the total amount of tokens", async function () {
        expect(await this.token.balanceOf(initialHolder)).to.be.bignumber.equal(new BN(initialSupply).addn(adjustment));
      });
    });
  });

  describe("transfer", function () {
    shouldBehaveLikeERC20Transfer(errorPrefix, initialHolder, recipient, initialSupply,
      function (from, to, value) {
        return this.token.transfer(to, value, { from });
      }, adjustment,
    );
  });

  describe("transfer from", function () {
    const spender = recipient;

    describe("when the token owner is not the zero address", function () {
      const tokenOwner = initialHolder;

      describe("when the recipient is not the zero address", function () {
        const to = anotherAccount;

        describe("when the spender has enough approved balance", function () {
          beforeEach(async function () {
            await this.token.approve(spender, initialSupply, { from: initialHolder });
          });

          describe("when the token owner has enough balance", function () {
            const amount = initialSupply;

            it("transfers the requested amount", async function () {
              await this.token.transferFrom(tokenOwner, to, amount, { from: spender });

              expect(await this.token.balanceOf(tokenOwner)).to.be.bignumber.equal(new BN("0").addn(adjustment).addn(adjustment));
              expect(await this.token.balanceOf(to)).to.be.bignumber.equal(new BN(amount).addn(adjustment));
            });

            it("decreases the spender allowance", async function () {
              await this.token.transferFrom(tokenOwner, to, amount, { from: spender });

              expect(await this.token.allowance(tokenOwner, spender)).to.be.bignumber.equal(new BN("0").addn(upgradedallowance).addn(adjustment));
            });

            it("emits a transfer event", async function () {
              const logs = await this.token.transferFrom(tokenOwner, to, amount, { from: spender });

              expectEvent(logs, "Transfer", {
                from: tokenOwner,
                to: to,
                value: amount,
              });
            });

            it("emits an approval event", async function () {
              const logs = await this.token.transferFrom(tokenOwner, to, amount, { from: spender });

              expectEvent(logs, "Approval", {
                owner: tokenOwner,
                spender: spender,
                value: (await this.token.allowance(tokenOwner, spender)).subn(upgradedallowance),
              });
            });
          });

          describe("when the token owner does not have enough balance", function () {
            const amount = initialSupply.addn(1);

            it("reverts", async function () {
              await expectRevert(this.token.transferFrom(
                tokenOwner, to, amount, { from: spender }), `${errorPrefix}: transfer amount exceeds balance`,
              );
            });
          });
        });

        describe("when the spender does not have enough approved balance", function () {
          beforeEach(async function () {
            await this.token.approve(spender, initialSupply.subn(1), { from: tokenOwner });
          });

          describe("when the token owner has enough balance", function () {
            const amount = initialSupply;

            it("reverts", async function () {
              await expectRevert(this.token.transferFrom(
                tokenOwner, to, amount, { from: spender }), `${errorPrefix}: transfer amount exceeds allowance`,
              );
            });
          });

          describe("when the token owner does not have enough balance", function () {
            const amount = initialSupply.addn(1);

            it("reverts", async function () {
              await expectRevert(this.token.transferFrom(
                tokenOwner, to, amount, { from: spender }), `${errorPrefix}: transfer amount exceeds balance`,
              );
            });
          });
        });
      });

      describe("when the recipient is the zero address", function () {
        const amount = initialSupply;
        const to = ZERO_ADDRESS;

        beforeEach(async function () {
          await this.token.approve(spender, amount, { from: tokenOwner });
        });

        it("reverts", async function () {
          await expectRevert(this.token.transferFrom(
            tokenOwner, to, amount, { from: spender }), `${errorPrefix}: transfer to the zero address`,
          );
        });
      });
    });

    describe("when the token owner is the zero address", function () {
      const amount = 0;
      const tokenOwner = ZERO_ADDRESS;
      const to = recipient;

      it("reverts", async function () {
        await expectRevert(this.token.transferFrom(
          tokenOwner, to, amount, { from: spender }), `${errorPrefix}: transfer from the zero address`,
        );
      });
    });
  });

  describe("approve", function () {
    shouldBehaveLikeERC20Approve(errorPrefix, initialHolder, recipient, initialSupply,
      function (owner, spender, amount) {
        return this.token.approve(spender, amount, { from: owner });
      }, adjustment, upgradedallowance
    );
  });
}

function shouldBehaveLikeERC20Transfer(errorPrefix, from, to, balance, transfer, adjustment) {
  describe("when the recipient is not the zero address", function () {
    describe("when the sender does not have enough balance", function () {
      const amount = balance.addn(1);

      it("reverts", async function () {
        await expectRevert(transfer.call(this, from, to, amount),
          `${errorPrefix}: transfer amount exceeds balance`,
        );
      });
    });

    describe("when the sender transfers all balance", function () {
      const amount = balance;

      it("transfers the requested amount", async function () {
        // token1 = 10000, to = 0, amount = 10000
        // token1 = 1000, to = 0, amount = 1000
        await transfer.call(this, from, to, amount);

        expect(await this.token.balanceOf(from)).to.be.bignumber.equal((new BN("0")).addn(adjustment).addn(adjustment));
        expect(await this.token.balanceOf(to)).to.be.bignumber.equal(new BN(amount).addn(adjustment));
      });

      it("emits a transfer event", async function () {
        const logs = await transfer.call(this, from, to, amount);

        expectEvent(logs, "Transfer", {
          from,
          to,
          value: amount,
        });
      });
    });

    describe("when the sender transfers zero tokens", function () {
      const amount = new BN("0");

      it("transfers the requested amount", async function () {
        await transfer.call(this, from, to, amount);

        expect(await this.token.balanceOf(from)).to.be.bignumber.equal(new BN(balance).addn(adjustment).addn(adjustment));
        expect(await this.token.balanceOf(to)).to.be.bignumber.equal(new BN("0").addn(adjustment));
      });

      it("emits a transfer event", async function () {
        const logs = await transfer.call(this, from, to, amount);

        expectEvent(logs, "Transfer", {
          from,
          to,
          value: amount,
        });
      });
    });
  });

  describe("when the recipient is the zero address", function () {
    it("reverts", async function () {
      await expectRevert(transfer.call(this, from, ZERO_ADDRESS, balance),
        `${errorPrefix}: transfer to the zero address`,
      );
    });
  });
}

function shouldBehaveLikeERC20Approve(errorPrefix, owner, spender, supply, approve, adjustment, upgradedallowance) {
  describe("when the spender is not the zero address", function () {
    describe("when the sender has enough balance", function () {
      const amount = supply;

      it("emits an approval event", async function () {
        const logs = await approve.call(this, owner, spender, amount);

        expectEvent(logs, "Approval", {
          owner: owner,
          spender: spender,
          value: amount,
        });
      });

      describe("when there was no approved amount before", function () {
        it("approves the requested amount", async function () {
          await approve.call(this, owner, spender, amount);

          expect(await this.token.allowance(owner, spender)).to.be.bignumber.equal(new BN(amount).addn(upgradedallowance));
        });
      });

      describe("when the spender had an approved amount", function () {
        beforeEach(async function () {
          await approve.call(this, owner, spender, new BN(1));
        });

        it("approves the requested amount and replaces the previous one", async function () {
          await approve.call(this, owner, spender, amount);

          expect(await this.token.allowance(owner, spender)).to.be.bignumber.equal(new BN(amount).addn(upgradedallowance));
        });
      });
    });

    describe("when the sender does not have enough balance", function () {
      const amount = supply.addn(1);

      it("emits an approval event", async function () {
        const logs = await approve.call(this, owner, spender, amount);

        expectEvent(logs, "Approval", {
          owner: owner,
          spender: spender,
          value: amount,
        });
      });

      describe("when there was no approved amount before", function () {
        it("approves the requested amount", async function () {
          await approve.call(this, owner, spender, amount);

          expect(await this.token.allowance(owner, spender)).to.be.bignumber.equal(new BN(amount).addn(upgradedallowance));
        });
      });

      describe("when the spender had an approved amount", function () {
        beforeEach(async function () {
          await approve.call(this, owner, spender, new BN(1));
        });

        it("approves the requested amount and replaces the previous one", async function () {
          await approve.call(this, owner, spender, amount);

          expect(await this.token.allowance(owner, spender)).to.be.bignumber.equal(new BN(amount).addn(upgradedallowance));
        });
      });
    });
  });

  describe("when the spender is the zero address", function () {
    it("reverts", async function () {
      await expectRevert(approve.call(this, owner, ZERO_ADDRESS, supply),
        `${errorPrefix}: approve to the zero address`,
      );
    });
  });
}

function upgradableTests(updatedSupply, initialHolder, recipient, adjustment, upgradedallowance, additionMint) {
  it("Test totalSupply", async function () {
    expect(await this.token.totalSupply()).to.be.bignumber.equal(new BN(updatedSupply).addn(adjustment));
  });

  it("Allowance", async function () {
    //it will always returned 100 as it is hardcoded.
    expect(await this.token.allowance(initialHolder, recipient)).to.be.bignumber.equal(new BN(100));
    expect(await this.token.allowance(recipient, initialHolder)).to.be.bignumber.equal(new BN(100));

    const approveEvent = await this.token.approve(recipient, 1000, { from: initialHolder });
    expectEvent(approveEvent, "Approval");

    expect(await this.token.allowance(initialHolder, recipient)).to.be.bignumber.equal(new BN(1000).addn(upgradedallowance));
    expect(await this.token.allowance(recipient, initialHolder)).to.be.bignumber.equal(new BN(100));
  });

  it("IncreaseAllowance", async function () {
    //it will always returned 100 as it is hardcoded.
    expect(await this.token.allowance(initialHolder, recipient)).to.be.bignumber.equal(new BN(100));
    expect(await this.token.allowance(recipient, initialHolder)).to.be.bignumber.equal(new BN(100));

    const approveEvent = await this.token.increaseAllowance(recipient, 1000, { from: initialHolder });
    expectEvent(approveEvent, "Approval");

    expect(await this.token.allowance(initialHolder, recipient)).to.be.bignumber.equal(new BN(1000).addn(upgradedallowance));
    expect(await this.token.allowance(recipient, initialHolder)).to.be.bignumber.equal(new BN(100));
  });

  it("DecreaseAllowance", async function () {
    expect(await this.token.allowance(initialHolder, recipient)).to.be.bignumber.equal(new BN(100));
    expect(await this.token.allowance(recipient, initialHolder)).to.be.bignumber.equal(new BN(100));

    const approveEvent = await this.token.approve(recipient, 1000, { from: initialHolder });
    expectEvent(approveEvent, "Approval");

    const decreaseEvent = await this.token.decreaseAllowance(recipient, 1000, { from: initialHolder });
    expectEvent(decreaseEvent, "Approval");

    expect(await this.token.allowance(initialHolder, recipient)).to.be.bignumber.equal(new BN(1000).addn(upgradedallowance));
    expect(await this.token.allowance(recipient, initialHolder)).to.be.bignumber.equal(new BN(100));
  });
}


function negativeTests(contract, accounts) {
  describe("Negative Test Cases", async function () {
    describe("Transfer negative test cases", async function () {
      it("When approve amount is negative", async function () {
        const approveAmount = -100;
        await expectRevert(
          this.token.approve(accounts[3], approveAmount, { from: accounts[0] }),
          `value out-of-bounds (argument="amount", value=${approveAmount}, code=INVALID_ARGUMENT, version=abi/5.0.7)`,
        );
      });

      it("When transfer amount is negative", async function () {
        const transferAmount = -20;
        await expectRevert(
          this.token.transfer(accounts[4], transferAmount, { from: accounts[0] }),
          `value out-of-bounds (argument="amount", value=${transferAmount}, code=INVALID_ARGUMENT, version=abi/5.0.7)`,
        );
      });

      it("When transfer amount is in decimals", async function () {
        const transferAmount = 20.2;
        await expectRevert(
          this.token.transfer(accounts[4], transferAmount, { from: accounts[0] }),
          `underflow (fault="underflow", operation="BigNumber.from", value=${transferAmount}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
        );
      });

      it("When transfer amount is a big Number", async function () {
        const transferAmount = 10e15;
        await expectRevert(
          this.token.transfer(accounts[4], transferAmount, { from: accounts[0] }),
          `overflow (fault="overflow", operation="BigNumber.from", value=${transferAmount}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
        );
      });

      it("When transfer from amount is negative", async function () {
        const transferAmount = -20;
        await expectRevert(
          this.token.transferFrom(accounts[0], accounts[3], transferAmount, { from: accounts[3] }),
          `value out-of-bounds (argument="amount", value=${transferAmount}, code=INVALID_ARGUMENT, version=abi/5.0.7)`,
        );
      });

      it("When transfer from amount is in decimals", async function () {
        const transferAmount = 20.2;
        await expectRevert(
          this.token.transferFrom(accounts[0], accounts[3], transferAmount, { from: accounts[3] }),
          `underflow (fault="underflow", operation="BigNumber.from", value=${transferAmount}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
        );
      });

      it("When transfer from amount is a Big Number", async function () {
        const transferAmount = 10e15;
        await expectRevert(
          this.token.transferFrom(accounts[0], accounts[3], transferAmount, { from: accounts[3] }),
          `overflow (fault="overflow", operation="BigNumber.from", value=${transferAmount}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
        );
      });

      it("When transfers the tokens From ZERO address", async function () {
        const transferAmount = 1000;
        await expectRevert(
          this.token.transferFrom(ZERO_ADDRESS, accounts[3], transferAmount, { from: accounts[3] }),
          `ERC20: transfer from the zero address`,
        );
      });

      it("When transfers the tokens To the ZERO address", async function () {
        const transferAmount = 1000;
        await expectRevert(
          this.token.transferFrom(accounts[1], ZERO_ADDRESS, transferAmount, { from: accounts[3] }),
          `ERC20: transfer to the zero address`,
        );
      });
    });

    describe("Mint negative test cases", async function () {
      it("When Mint amount is negative", async function () {
        const mintAmount = -100;
        await expectRevert(
          this.token.mint(accounts[0], mintAmount, { from: accounts[0] }),
          `value out-of-bounds (argument="amount", value=${mintAmount}, code=INVALID_ARGUMENT, version=abi/5.0.7)`,
        );
      });
      it("Minting of token should not be processed when the public address is blank ", async function () {
        await expectRevert(
          this.token.mint("", 100),
          `invalid address (argument="address", value="", code=INVALID_ARGUMENT, version=address/5.5.0) (argument="account", value="", code=INVALID_ARGUMENT, version=abi/5.0.7)`
        );
      });
      it("When mint amount is a Big Number", async function () {
        const mintAmount = 10e15;
        await expectRevert(
          this.token.mint(accounts[0], mintAmount, { from: accounts[0] }),
          `overflow (fault="overflow", operation="BigNumber.from", value=${mintAmount}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
        );
      });
    });

    describe("Burn negative test cases", async function () {
      it("When Burn amount is negative", async function () {
        const burnAmount = -1000;
        if(contract == "ERC20") {
          await expectRevert(
            this.token.burn(accounts[0], burnAmount),
            `value out-of-bounds (argument="amount", value=${burnAmount}, code=INVALID_ARGUMENT, version=abi/5.0.7)`,
          );
        } else {
          await expectRevert(
            this.token.burn(burnAmount, { from: accounts[0] }),
            `value out-of-bounds (argument="amount", value=${burnAmount}, code=INVALID_ARGUMENT, version=abi/5.0.7)`,
          );
        }
      });

      it("When burn amount is a big number", async function () {
        const burnAmount = 10e15;
        if(contract == "ERC20") {
          await expectRevert(
            this.token.burn(accounts[0], burnAmount),
            `overflow (fault="overflow", operation="BigNumber.from", value=${burnAmount}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
          );
        } else {
          await expectRevert(
            this.token.burn(burnAmount, { from: accounts[0] }),
            `overflow (fault="overflow", operation="BigNumber.from", value=${burnAmount}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
          );
        }
      });
    });

    describe("Allowance negative test cases", async function () {
      it("Approve amount is negative ", async function () {
        const approveAmount = -100;
        await expectRevert(
             this.token.approve(accounts[1], approveAmount, { from: accounts[0] }),
            `value out-of-bounds (argument="amount", value=${approveAmount}, code=INVALID_ARGUMENT, version=abi/5.0.7)`
            
        );
      });

      it("Increase Allowance amount is negative ", async function () {
        const allowanceAmount = -100;
          await expectRevert(
               this.token.increaseAllowance(accounts[1], allowanceAmount, { from: accounts[0] }),
              `value out-of-bounds (argument="addedValue", value=${allowanceAmount}, code=INVALID_ARGUMENT, version=abi/5.0.7)`
          );
      });

      it("Decrease Allowance amount is negative", async function () {
        const allowanceAmount = -100;
          await expectRevert(
               this.token.decreaseAllowance(accounts[1], allowanceAmount, { from: accounts[0] }),
              `value out-of-bounds (argument="subtractedValue", value=${allowanceAmount}, code=INVALID_ARGUMENT, version=abi/5.0.7)`,            
          );
      });

      it("When increase allowance amount is a big Number", async function () {
        const allowanceAmount = 10e15;
        await expectRevert(
            this.token.increaseAllowance(accounts[1], allowanceAmount, { from: accounts[0] }),
            `overflow (fault="overflow", operation="BigNumber.from", value=${allowanceAmount}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
          );
      });

      it("When decrease allowance amount is a big Number", async function () {
        const allowanceAmount = 10e15;
        await expectRevert(
            this.token.decreaseAllowance(accounts[1], allowanceAmount, { from: accounts[0] }),
            `overflow (fault="overflow", operation="BigNumber.from", value=${allowanceAmount}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
        );
      });

      it("When approve amount is in decimal ", async function () {
        const approveAmount = 100.50;
        await expectRevert(
             this.token.approve(accounts[1], approveAmount, { from: accounts[0] }),
            `underflow (fault="underflow", operation="BigNumber.from", value=${approveAmount}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,   
        );
      });

      it("When increase allowance amount is in decimal", async function () {
        const floatAllowanceAmount = 1500.90;
        await expectRevert(
            this.token.decreaseAllowance(accounts[1], floatAllowanceAmount, { from: accounts[0] }),
           `underflow (fault="underflow", operation="BigNumber.from", value=${floatAllowanceAmount}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
        );
      });

      it("When decrease allowance amount is in decimal", async function () {
        const floatAllowanceAmount = 1500.90;
        await expectRevert(
            this.token.decreaseAllowance(accounts[1], floatAllowanceAmount, { from: accounts[0] }),
            `underflow (fault="underflow", operation="BigNumber.from", value=${floatAllowanceAmount}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
        );
      });
    });
  });
}

module.exports = {
  shouldBehaveLikeERC20,
  shouldBehaveLikeERC20Transfer,
  shouldBehaveLikeERC20Approve,
  upgradableTests,
  negativeTests,
};