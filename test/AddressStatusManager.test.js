const { BN, constants, expectEvent, expectRevert } = require("@openzeppelin/test-helpers");
const { ZERO_ADDRESS } = constants;

const { expect } = require("chai");

const AddressStatusManager = artifacts.require("AddressStatusManager");

contract("AddressStatusManager", accounts => {
    beforeEach(async () => {
        this.manager = await AddressStatusManager.new({ from: accounts[0] });
    });

    it("Test Check Owner", async () => {
        expect(await this.manager.contractOwner()).to.equal(accounts[0]);
    });
    it("Test set status as owner", async () => {
        const receipt = await this.manager.setStatus(
            accounts[1],
            new BN("1"),
            { from: accounts[0] }
        );
        expectEvent(receipt, "ChangeAddressStatus");

        expect(await this.manager.statusOf(accounts[1])).to.be.bignumber.equal(new BN("1")); 
    });
    it("Test set status as non-owner", async () => {
        await expectRevert(
            this.manager.setStatus(accounts[2], new BN("1"),  { from: accounts[1] }),
            "Ownable: Only the owner can call this",
        );
    });
    it("Get status of non-existing account", async () => {
        expect(await this.manager.statusOf(accounts[1])).to.be.bignumber.equal(new BN("0")); 
    });
});
