const { constants, expectEvent, expectRevert, BN } = require("@openzeppelin/test-helpers");
const { ZERO_ADDRESS } = constants;

const { expect } = require("chai");

const CustomSafeMath = artifacts.require("CustomSafeMathMock");

contract("Ownable", accounts => {
    beforeEach(async () => {
        this.math = await CustomSafeMath.new({ from: accounts[0] });
    });

    it("Convert to unsigned int", async () => {
        expect(await this.math.toUint256Safe(0)).to.be.bignumber.equal(new BN("0"));
        expect(await this.math.toUint256Safe(100)).to.be.bignumber.equal(new BN("100"));
        expect(await this.math.toUint256Safe(123456789)).to.be.bignumber.equal(new BN("123456789"));

        await expectRevert(
            this.math.toUint256Safe(-100),
            "int should be > 0"
        );
    });

    it("Convert to signed int", async () => {
        expect(await this.math.toInt256Safe(0)).to.be.bignumber.equal(new BN("0"));
        expect(await this.math.toInt256Safe(100)).to.be.bignumber.equal(new BN("100"));
        expect(await this.math.toInt256Safe(123456789)).to.be.bignumber.equal(new BN("123456789"));

        const overflow = 2**255 + 1;

        await expectRevert(
            this.math.toInt256Safe(overflow),
            `overflow (fault="overflow", operation="BigNumber.from", value=${overflow}, code=NUMERIC_FAULT, version=bignumber/5.5.0)`,
        );
    });
});
 