async function deploy(contractName, ...args) {
    console.log(`Deploying ${contractName}... on ${hre.hardhatArguments.network}`);
    const classVal = await ethers.getContractFactory(contractName);
    const instance = await classVal.deploy(...args);
    console.log(`${contractName} deployed to ${instance.address}`);
    return instance;
}

/**
 * Tokens consists of an array of dictionaries, each containing a:
 * - name
 * - symbol
 * - initialSupply
 */
async function deployTokens(tokens, addressManager, feeCalculator) {
    let result = [];
    for (const x of tokens) {
        const instance = await deploy(
            "Token",
            x["name"],
            x["symbol"],
            x["initialSupply"],
            addressManager.address,
            "0xe81106437F85d77ED187D72aC5844BD11c47f5EA",
        );
        result.push(instance);
    }
    return result;
}

async function migration() {
    // const feeCalculator = await deploy("FeeCalculator");
    const addressManager = await deploy("AddressStatusManager");

    const tokens = await deployTokens([
        {
            name: "24G Gold Token",
            symbol: "GLDTF",
            initialSupply: 10000 // 10,000g
        },
        {
            name: "24G Silver Token",
            symbol: "SLVTF",
            initialSupply: 100000 // 100,000g
        },
        {
            name: "24G Platinum Token",
            symbol: "PLTTF",
            initialSupply: 9000 // 9,000g
        }
    ], addressManager, null);
}

async function main() {
    await migration();
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });