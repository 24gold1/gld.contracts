npx hardhat flatten $1 | grep -v SPDX-License > temp.sol
myth analyze temp.sol --max-depth $2 || exit 1
rm temp.sol